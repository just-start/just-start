import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.recommended,
  {
    ignores: [
      "*.md",
      "./dist/",
      "./gulpfile.js",
      "./node_modules/",
      "**/service-worker.js",
      "./src/static/",
      "./vendor/",
      "./vite.config.js"
    ]
  }
);
