# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed

- Use modules in package.json
- Use Node 22
- Migrate from NPM to bun because it is faster
- Replace prettier-plugin-twig-melody with @nsetyo/prettier-plugin-twig
- Upgrade prettier, stylelint and eslint
- Updated readme
- Updated Gulp to 5

### Added

- Integration of Speculation Rules API
- htaccess
- Translated pages for default routes
- Sourcemaps for SCSS in development mode
- Hot reloading for JS and CSS in development mode

## [v3.0.0] - 2023-03-13

### Changed

- Replaced _Webpack_ with _Vite_
- Stylelint so we can use `use`, `math.div` and `percentage` from `sass`.
- Sorted stuff alphabetically

### Added

- Support for global imports (`*`) in .scss files
- postversion hook
- Redundant CSS rules are now being combined by css-purge

## [v2.0.0] - 2022-08-13

### Added

- Twig and JSON data (which makes https://gitlab.com/the-static-boilerplate/core redundant and therefore deprecated). The setup does not force you to use Twig, you can still place static HTML files in [src/static](src/static)

### Changed

- Upgraded node modules

## [v1.3.0] - 2022-07-03

### Added

- Support for Progressive Web Application (PWA)
  - [src/static/offline.html](src/static/offline.html)
  - [src/static/vendor/service-worker.js](src/static/vendor/service-worker.js)
  - [src/index.ts](src/index.ts)
- Favicons
- Basic structure for _SCSS_ and _TS_

### Changed

- Updated browserslist to [default settings](https://github.com/browserslist/browserslist)
- Upgraded required node version from `12` to `16`
- Upgraded required npm version from `6` to `8`
- Updated [README.md](README.md)
- Remove directories `'src/static'` from [.prettierignore.json](.prettierignore.json). Now `npm run cqs:html` actually works. Place stuff you don't want to be prettiefied in a directory `'vendor'`
- Exlude directories `'vendor'` and `'node_modules'` in [.prettierrc.json](.prettierrc.json) and [.eslintignore](.eslintignore)

### Fixed

- _Prettier_ can now search subdirectories properly
- _Stylelint_ now works with `postcss-scss` and can format _SCSS_ code properly

## [v1.2.0] - 2022-03-28

### Changed

- Replace deprecated [node-sass](https://www.npmjs.com/package/node-sass) with [sass](https://www.npmjs.com/package/sass)
- Minification of CSS is handled by [node-sass](https://www.npmjs.com/package/node-sass)
- Minification of JavaScript is handled by [typescript](https://www.npmjs.com/package/typescript)
- Minification of HTML is handled by [html-minifier-terser](https://www.npmjs.com/package/html-minifier-terser)
- Upgraded dependencies to latest versions

## [v1.1.0] - 2021-05-13

### Fixed

- stylelint's rule `'no-invalid-position-at-import-rule'` caused trouble when using `@import` inside of another element

  Before:

  ```scss
  @import "foo";

  body {
    background-color: red;
  }
  ```

  After:

  ```scss
  body {
    @import "foo";

    background-color: red;
  }
  ```

### Added

- HTML is being minified on build
- Task `npm run build`

### Changed

- Upgraded dependencies
- Updated [README.md](README.md)
- Updated eslintrc.json to work better with TypeScript
- Upgraded node modules
- TypeScript compilation is much faster
- Sourcemaps (.ts, .scss) built only when running `npm start` not in `npm run build`

## [v1.0.0] - 2021-05-13

### Added

- Initial release
- Watch and compile JavaScript and TypeScript to browser readable JavaScript
- Watch and compile SCSS to CSS (minified)
- Watch and transfer files from src/static to dist
- Code Quality Standards (automated linting and formatting)
- MacOS notifications

[unreleased]: https://gitlab.com/just-start/just-start
[v3.0.0]: https://gitlab.com/just-start/just-start/-/compare/v2.0.0...v3.0.0
[v2.0.0]: https://gitlab.com/just-start/just-start/-/compare/v1.3.0...v2.0.0
[v1.3.0]: https://gitlab.com/just-start/just-start/-/compare/v1.2.0...v1.3.0
[v1.2.0]: https://gitlab.com/just-start/just-start/-/compare/v1.1.0...v1.2.0
[v1.1.0]: https://gitlab.com/just-start/just-start/-/compare/v1.0.0...v1.1.0
[v1.0.0]: https://gitlab.com/just-start/just-start/-/tree/v1.0.0
