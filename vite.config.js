import browserslistToEsbuild from 'browserslist-to-esbuild'
import ViteRestart from 'vite-plugin-restart';
import sassGlobImports from 'vite-plugin-sass-glob-import';
import { defineConfig } from 'vite';
import autoprefixer from 'autoprefixer';

export default defineConfig({
  esbuild: {
    /**
     * Prevents ESBuild to throw when using a feature not supported by the
     * list of supported browsers coming from the `browserslist` file.
     */
    supported: {
      'top-level-await': true,
    },
  },
  publicDir: './src/static',
  server: {
    /** Make sure the port is the same in src/components/dom/index.twig. This enabled (hot) reloading of TypeScript and SCSS */
    port: 5174
  },
  build: {
    target: browserslistToEsbuild(),
    sourcemap: true,
    /** Set the path to the distribution */
    outDir: './dist',
    emptyOutDir: true,
    rollupOptions: {
      /** Ensure we do not produce filenames with hashes */
      output: {
        entryFileNames: `assets/[name].js`,
        chunkFileNames: `assets/[name].js`,
        assetFileNames: `assets/[name].[ext]`
      }
    }
  },
  css: {
    devSourcemap: true, // does not do anything because we do not use vite in def mode but only vite build
    postcss: {
      plugins: [
        /** Prefix CSS */
        autoprefixer({}), // add options if needed
      ]
    }
  },
  plugins: [
    sassGlobImports(),

    /** Enable reloading when changing any files */
    ViteRestart({
      reload: ['**/src/**/*.(twig|php|json)'],
    })
  ]
});
