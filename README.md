# Just Start

| Input                                                            | Output                                                        |
| ---------------------------------------------------------------- | ------------------------------------------------------------- |
| ![Headaches](./readme/christian-erfurt-sxQz2VfoFBE-unsplash.jpg) | ![No headaches](./readme/bruce-mars-AndE50aaHn4-unsplash.jpg) |
| TypeScript                                                       | JavaScript                                                    |
| SCSS                                                             | CSS                                                           |
| Twig (with JSON)                                                 | HTML                                                          |

## Table of contents

<!-- TOC -->

- [Just Start](#just-start)
  - [Table of contents](#table-of-contents)
  - [Develop](#develop)
  - [Dependencies \& Requirements](#dependencies--requirements)
  - [What is new?](#what-is-new)
  - [What it does](#what-it-does)
    - [Static files](#static-files)
    - [Stylesheets](#stylesheets)
      - [Linting and formatting](#linting-and-formatting)
      - [Compiling](#compiling)
    - [JavaScripts](#javascripts)
      - [Linting and formatting](#linting-and-formatting-1)
      - [Compiling](#compiling-1)
    - [Data](#data)
      - [Global data](#global-data)
      - [Sections](#sections)
      - [Section rendering](#section-rendering)
  - [Supported Browsers](#supported-browsers)
  - [Formatting and linting](#formatting-and-linting)
  - [How good is it?](#how-good-is-it)

<!-- /TOC -->

---

## Develop

```bash
# compile and watch including source-maps
bun run dev

# build (no source-maps, minified)
bun run build
```

## Dependencies & Requirements

- [Node.js](https://nodejs.org/en/)
- [bun](https://bun.sh/)

## What is new?

See [changelog.md](changelog.md)

## What it does

- Watch
- Compile
- Minify
- Lint
- Format
- _Progressive Web App_ (PWA) ready (see [src/static/manifest.json](src/static/manifest.json) and [src/static/service-worker.js](src/static/service-worker.js))

### Static files

All files in [src/static](src/static) will be copied to [dist](dist).

### Stylesheets

#### Linting and formatting

Uses stylelint (see [.stylelintrc.cjs](.stylelintrc.cjs), [.stylelintignore](.stylelintignore), [.prettierrc.json](.prettierrc.json) and [.prettierignore](.prettierignore))).

#### Compiling

_SCSS_ will be compiled to _CSS_.

| Input                            | Output                                                                                               |
| -------------------------------- | ---------------------------------------------------------------------------------------------------- |
| [src/index.scss](src/index.scss) | [dist/assets/index.css](dist/assets/index.css), [dist/assets/index.css.map](dist/assets/index.css.map) (only during development, currently not available: https://github.com/vitejs/vite/issues/2830) |

### JavaScripts

#### Linting and formatting

Uses eslint (see [eslintrc.config.js](eslintrc.config.js), [.prettierrc.json](.prettierrc.json) and [.prettierignore](.prettierignore)).

#### Compiling

_TypeScript_ will be compiled to _JavaScript_. See [tsconfig.json](tsconfig.json) and [vite.config.js](vite.config.js).

| Input                            | Output                                                                                               |
| -------------------------------- | ---------------------------------------------------------------------------------------------------- |
| [src/main.ts](src/main.ts) | [dist/assets/index.js](dist/assets/index.js), [dist/assets/index.js.map](dist/assets/index.js.map) (only during development) |

### Data

Similar to a CMS (Content Management System), data can be stored in _JSON_ files located in [src/data](src/data).

#### Global data

Data in [src/data/global.json](src/data/global.json) can be accesssed in every _Twig_ and _TypeScript_ file. In _Twig_ the variable `global` is reserved for the contents of this file (see [gulpfile.cjs](gulpfile.cjs)).

```twig
{{ global.foo }}
```

```ts
import * as globalData from './data/global.json';
console.log(globalData.foo)
```

#### Sections

Data may be organized in sections.

Data from a section (e.g. [src/data/sections/de-error.json](src/data/sections/de-error.json)) is also available in the [Global data](#global-data) (not the _camelCase_ spelling):

```twig
{{ global.deError[0].title }} → "Error 404"
```

With _TypeScript_ you need to load the data for a section seperately, it is not included in the global data.

```ts
import * as deError from './data/sections/de-error.json';
console.log(deError.rows[0].data.title)
```

Data that is organized in sections can be rendered through Twig as HTML.

#### Section rendering

You may create templates for different pages and/or page-types in [src/templates](src/templates). The templates can be filled with data. The data is stored in [src/data](src/data), for example in [src/data/sections/de-error.json](src/data/sections/de-error.json).

If you add the correct values for `template` and `route` to a section, each item in `rows` will be rendered as a single _HTML_ file in [./dist](./dist):

```json
{
  "template": "@src/templates/error/index.twig",
  "route": "{slug}",
  "rows": [
    {
      "slug": "404",
      "data": {
        "title": "Error 404"
      }
    }
  ]
}
```

A more complex example with a deeplink

```json
{
  "template": "@src/templates/user/index.twig",
  "route": "user/{slug}",
  "rows": [
    {
      "slug": "max-musterfrau",
      "data": {
        "title": "Max Musterfrau"
      }
    }
  ]
}
```

_Note:_ Use _namespaced path_ `@src/**/.twig` (@see [Twig Namespaces](#twig-namespaces)) (not required).

Within each _Twig_ file you may use the reserved variable `this` to access data from a section, that is only available for the currently rendered item from that section. Like a page or a blog post in a Content Management System.

```twig
{{ this.title }} → Error 404
```

## Supported Browsers

Target browsers are defined in [vite.config.js](vite.config.js) at `legacy`.

## Formatting and linting

Code is automatically linted and formatted when running a _Git_ commit.

- [Stylelint](https://stylelint.io/)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Lint-Staged](https://www.npmjs.com/package/lint-staged)
- [Husky](https://www.npmjs.com/package/husky)

## How good is it?

Google Lighthouse results say „400/400”:

![Lighthouse results](./readme/lighthouse-results.jpg)
