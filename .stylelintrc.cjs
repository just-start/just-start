module.exports = {
  extends: ['stylelint-config-recommended', 'stylelint-prettier/recommended'],
  plugins: [
    'stylelint-scss',
    'stylelint-order'
  ],
  rules: {
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
    'no-invalid-position-at-import-rule': null,
    'scss/at-mixin-argumentless-call-parentheses': 'always',
    'scss/at-mixin-parentheses-space-before': 'never',
    'media-query-no-invalid': null,
    // Rule to handle imports without empty lines between them
    'at-rule-empty-line-before': [
      'always',
      {
        except: ['blockless-after-same-name-blockless', 'first-nested'],
      }
    ],
    'declaration-empty-line-before': [
      'always',
      {
        except: ['after-declaration', 'first-nested']
      }
    ],
    'no-descending-specificity': null,
    'function-no-unknown': [
      true,
      {
        ignoreFunctions: [
          'baseline',
          'div',
          'grid-columns',
          'inspect',
          'math.div',
          'percentage'
        ]
      }
    ],
    // Enforce the order of SCSS, imports, custom properties, etc.
    'order/order': [
      'dollar-variables',          // SCSS/Less variables
      'custom-properties',         // Custom properties (CSS variables)
      {
        type: 'at-rule',
        name: 'include',           // SCSS mixin includes
      },
      {
        type: 'at-rule',
        name: 'import',          // Import other scss files without spaces
      },
      'at-variables',              // SCSS `@` variables
      'declarations',              // Regular CSS declarations
      {
        type: 'at-rule',
        name: 'media',             // Media queries at the bottom
      },
      'rules',                     // Regular rules (selectors)
      {
        type: 'at-rule',
        name: 'supports',          // Feature queries at the bottom
      },
      'at-rules'                   // Other at-rules not specified
    ],
    'order/properties-alphabetical-order': true,
    'rule-empty-line-before': [
      'always',
      {
        except: ['after-single-line-comment', 'first-nested'],
      }
    ],
    'scss/declaration-nested-properties': 'never'
  }
};
